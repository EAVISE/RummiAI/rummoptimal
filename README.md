# Rummoptimal


Find the most likely set of Rummikub tiles, based on prediction probabilities
and the rules of rummikub.
Uses the IDP-Z3 engine as the solver.

Works in three simple steps:

1. Generates an IDP specification, consisting of vocabulary, structure,
   theory and main based on the predictions.
2. Feeds this to the IDP-Z3 engine.
3. Parses IDP-Z3's output into a nice format.

Input should be a list of tiles, with each tile being represented by a dictionary containing prediction dictionaries (predictionaries? I'm going to coin that term!).

## Usage

```
from rummoptimal import optimal_set

predictions = [{'color': {'red': 0.8, 'blue':0.1}, 'number': {1: 0.8, 2: 0.05}},
               {'color': {'orange': 0.3, 'blue': 0.5}, 'number': {1: 0.6, 7: 0.4}},
               {'color': {'blue': 0.6, 'black: '0.3'}, 'number': {1: 0.2, 8: 0.6}}]
opt_set = optimal_set(predictions)

# Result: [{'color': 'red', 'number': 1}, {'color': 'orange', 'number': 1}, {'color': 'blue', 'number': 1}]
```

Author: Simon Vandevelde (s.vandevelde@kuleuven.be)
