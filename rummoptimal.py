"""
rummoptimal.py

Find the most likely set of Rummikub tiles, based on prediction probabilities
and the rules of rummikub.
Uses the IDP-Z3 engine as the solver.
Works in three simple steps:
    1. Generate an IDP specification, consisting of vocabulary, structure,
       theory and main based on the predictions.
    2. Feed this to the IDP-Z3 engine.
    3. Parse IDP-Z3's output into a nice format.

Author: Simon Vandevelde (s.vandevelde@kuleuven.be)
"""
import sys
import re
from io import StringIO
from idp_engine.Parse import idpparser

# The theory and main are always the same, so we just use a nice ol' constant.
THEORY_MAIN = """
theory T:V {
    // Set is either a run or a group.
    (run() & ~group()) | (~run() & group()).

    { // Jokers have tile nb 0
        !x in tile: joker(x) <- tile_nb(x) = 0.
    }

    // Correct run:
    // The nb difference between two tiles (not jokers) is their index difference.
    // I.e. for |2||3||4| -> (4 - 2) = 2 - 0 .
    // This is a safer notation in the presence of multiple jokers.
    !x in tile, y in tile: run() & ~joker(x) & ~joker(y) & x < y
                => (tile_nb(y) - tile_nb(x) = y - x)
                    & (tile_color(x) = tile_color(y)).

    // Correct group:
    // All tiles (that are not jokers) have the same number, but different colors.
    // Due to a bug in IDP-Z3, we have to formulate it as follows.
    // But this doesn't seem to lead to much slowdown.
    !x in tile, y in tile: group() & ~joker(x) & ~joker(y) & x < y
                    => tile_nb(x) = tile_nb(y)
                        & tile_color(x) ~= tile_color(y).

    // Calculate the total chance.
    total_chance() = sum{x in tile: true : chance_of_color(x, tile_color(x)) + chance_of_nb(x, tile_nb(x))}.
}

procedure main() {
    pretty_print(Problem(T, S).optimize("total_chance()", minimize=False, complete=True).assignments)
}

"""


def generate_voc(nb_tiles):
    """
    Create vocabulary specifically for a `nb_tiles` many tiles.
    Possible the silliest function ever created, but that's life. :-)
    """
    return f"""
    vocabulary V {{
        type tile := {{0..{nb_tiles-1}}}
        type nb := {{0..13}} // 0 represents joker
        type color := {{blue, red, orange, black}}
        joker: (tile) -> Bool
        chance_of_nb: (tile * nb) -> Real
        chance_of_color: (tile * color) -> Real
        tile_nb: (tile) -> nb
        tile_color: (tile) -> color
        total_chance: () -> Real
        run: () -> Bool
        group: () -> Bool
    }}
    """


def generate_struct(probs):
    """
    Generate a structure based on the NN's detected probabilities.
    This struct consists of our "input" values.
    We need to fill two functions: `chance_of_nb` and `chance_of_color`.
    """
    nb_str = "chance_of_nb := {\n"
    color_str = "chance_of_color := {\n"

    nb_probs = []
    color_probs = []
    for i, tile in enumerate(probs):
        for nb, prob in tile['number'].items():
            nb_probs.append(f"({i}, {nb}) -> {prob}")
        for color, prob in tile['color'].items():
            color_probs.append(f"({i}, {color}) -> {prob}")

    nb_str += ",\n".join(nb_probs) + "} else 0"
    color_str += ",\n".join(color_probs) + "} else 0"
    return f"structure S:V {{\n {nb_str}\n {color_str}\n }}"


def generate_spec(probs):
    """
    Generate an IDP specification based on the NN's detection probabilities.
    """
    voc = generate_voc(len(probs))
    struct = generate_struct(probs)

    return voc + struct + THEORY_MAIN


def run_IDP(spec):
    idp = idpparser.model_from_str(spec)

    # Change stdout to local variable (there's no better way of doing this).
    old_stdout = sys.stdout
    sys.stdout = idp_out = StringIO()

    # Run IDP-Z3.
    idp.execute()

    # Change stdout back to normal.
    sys.stdout = old_stdout
    idp_out = idp_out.getvalue()

    joker_ids = []
    tile_nb = []
    tile_color = []
    # Now we parse the output!
    for line in idp_out.split('\n'):
        if not joker_ids and line[0:5] == 'joker':
            joker_ids = re.findall(r'(\d*)->true', line)
            joker_ids = [int(x) for x in joker_ids]
        if not tile_nb and line[0:7] == 'tile_nb':
            tile_nb = re.findall(r'(\d*)->(\d*)', line)
            tile_nb = {int(x): int(y) for (x, y) in tile_nb}
        if not tile_color and line[0:10] == 'tile_color':
            tile_color = re.findall(r'(\d*)->(\w*)', line)
            tile_color = {int(x): y for (x, y) in tile_color}

    # Now convert to our output format.
    outp = []
    for i in range(len(tile_nb)):
        tile = {'color': '', 'number': ''}
        if i in joker_ids:
            tile['number'] = 0
        else:
            tile['number'] = tile_nb[i]
        tile['color'] = tile_color[i]
        outp.append(tile)
    return outp


def optimal_set(probs):
    """
    Based on the NN's predictions, finds the most likely Rummikub set.
    The predictions should be given as a list of tiles, with each tile
    consisting of a dict {'color': {}, 'number': {}}, and each sub-dict
    consisting of the value (i.e. 'blue' or 1) and the predicted probability.
    E.g.:
    ```
    [
      {'color': {'blue': 0.3, 'red': 0.7},
       'number': {1: 0.6, 7: 0.3, 9: 0.1}},
      .
      .
      .
    ]
    ```
    """
    # Find the NN's answer based on the best estimates
    NN_set = []
    for tile in probs:
        color = max(tile['color'].items(), key=lambda k: k[1])[0]
        number = max(tile['number'].items(), key=lambda k: k[1])[0]
        NN_set.append({'color': color, 'number': number})
    spec = generate_spec(probs)
    optimal_set = run_IDP(spec)
    if optimal_set != NN_set:
        for i in range(len(optimal_set)):
            if optimal_set[i]['color'] != NN_set[i]['color']:
                print(NN_set[i]['color'], '->', optimal_set[i]['color'])
            if optimal_set[i]['number'] != NN_set[i]['number']:
                print(f"{i}: {NN_set[i]['number']} -> {optimal_set[i]['number']}")
    return optimal_set


if __name__ == "__main__":
    # print(optimal_set([{'color': {'blue': 0.2, 'black': 0.1, 'red': 0.7},
    #                     'number': {0: 0.3, 6: 0.7}},
    #                    {'color': {'blue': 0.9, 'black': 0.1},
    #                     'number': {6: 0.7, 9: 0.3}},
    #                    {'color': {'blue': 0.9, 'black': 0.1},
    #                     'number': {7: 0.8, 4: 0.2}},
    #                    {'color': {'blue': 0.9, 'black': 0.1},
    #                     'number': {8: 0.6, 3: 0.4}}]))

    # print(optimal_set([{'color': {'red': 0.3, 'black': 0.1, 'blue': 0.5, 'orange': 0.3},
    #                     'number': {0: 0.01, 1: 0.1, 2: 0.05, 3: 0.6, 4: 0.2, 5: 0.2, 6: 0.4, 7: 0.1, 8: 0.5, 9: 0.2, 10: 0.02, 11: 0.03, 12: 0.6, 13: 0.5}},
    #                    {'color': {'red': 0.5, 'black': 0.2, 'blue': 0.05, 'orange': 0.15 },
    #                     'number': {0: 0.01, 1: 0.9, 2: 0.65, 3: 0.5, 4: 0.1, 5: 0.2, 6: 0.4, 7: 0.2, 8: 0.2, 9: 0.2, 10: 0.02, 11: 0.03, 12: 0.3, 13: 0.5}},
    #                    {'color': {'red': 0.5, 'black': 0.1, 'blue': 0.55, 'orange': 0.55 },
    #                     'number': {0: 0.01, 1: 0.4, 2: 0.75, 3: 0.25, 4: 0.01, 5: 0.3, 6: 0.4, 7: 0.1, 8: 0.3, 9: 0.2, 10: 0.02, 11: 0.03, 12: 0.4, 13: 0.5}},
    #                    {'color': {'red': 0.3, 'black': 0.1, 'blue': 0.05, 'orange': 0. },
    #                     'number': {0: 0.01, 1: 0.3, 2: 0.2, 3: 0.3, 4: 0.001, 5: 0.2, 6: 0.4, 7: 0.1, 8: 0.2, 9: 0.2, 10: 0.02, 11: 0.03, 12: 0.3, 13: 0.5}}]))

    # print(optimal_set([{'color': {'red': 0.4248446524143219, 'orange':
    #             0.41710084676742554, 'blue': 0.1580038219690323, 'black':
    #             5.0601451221155e-05}, 'number': {1: 0.9997395873069763, 2:
    #             5.111069789620615e-09, 3: 1.3534980699159843e-13, 4:
    #             0.0002425573766231537, 5: 6.440246878408043e-15, 6:
    #             1.4324942640087102e-05, 7: 3.601168145905831e-06, 8:
    #             1.205952547024547e-12, 9: 2.674579167752993e-19, 10:
    #             4.606020774927074e-25, 11: 7.700256543508033e-10, 12:
    #             8.40892845243069e-23, 13: 8.300433887780747e-25}}, {'color':
    #             {'red': 0.9999991655349731, 'orange': 7.050741284223006e-13,
    #             'blue': 7.202914389381476e-07, 'black':
    #             1.0925867854894022e-07}, 'number': {1:
    #             5.986134921087682e-10, 2: 0.9999479055404663, 3:
    #             3.3597785886740894e-08, 4: 3.5942732665716903e-06, 5:
    #             1.7375102743244497e-06, 6: 1.8821500347848685e-12, 7:
    #             1.7497621129791696e-08, 8: 4.669783083954826e-05, 9:
    #             1.1436619706817197e-10, 10: 3.0366275915673864e-19, 11:
    #             3.2060001838950705e-17, 12: 1.4989240959550898e-09, 13:
    #             2.0699731533881105e-13}}, {'color': {'red':
    #             0.9999972581863403, 'orange':
    #             1.1895795763369743e-12, 'blue':
    #             2.697487843761337e-06, 'black':
    #             3.59683680409284e-12}, 'number': {1:
    #             3.1693396897194412e-21, 2:
    #             2.924478870942643e-15, 3: 1.0, 4:
    #             6.773071901640867e-18, 5:
    #             1.9103829629329994e-09, 6:
    #             1.2812868543932154e-09, 7:
    #             4.157343907738593e-24, 8:
    #             1.1352025683407721e-13, 9:
    #             6.191524376530921e-11, 10:
    #             2.1774949654920935e-23, 11:
    #             1.0247471013437587e-35, 12:
    #             7.069446027467304e-26, 13:
    #             1.5113861581535616e-18}}]))

    print(optimal_set([{'color': {'red': 0.9998749494552612, 'orange':
        1.133171849687642e-06, 'blue': 0.00012363973655737936, 'black':
        2.1983423437177407e-07}, 'number': {1: 0.04250365495681763, 2:
            4.264427388989134e-07, 3: 8.903111847757827e-08, 4:
            0.0014715628931298852, 5: 8.85783180137878e-09, 6:
            0.0004277477855794132, 7: 0.9555943012237549, 8:
            9.009352197608678e-07, 9: 2.5647599599665227e-09, 10:
            1.4023099088183999e-12, 11: 1.2779130429407815e-06, 12:
            2.131726192051639e-13, 13: 1.6420972799082217e-17}}, {'color':
                {'red': 0.9999591112136841, 'orange': 4.794104668803811e-09,
                    'blue': 3.879497671732679e-05, 'black':
                    2.1717526124120923e-06}, 'number': {1:
                        6.625684032915556e-18, 2: 0.9807687401771545, 3:
                        2.8570175739095482e-11, 4: 2.9000077574892202e-06, 5:
                        2.235459214716684e-05, 6: 1.1445601140035871e-13, 7:
                        2.648634733759536e-07, 8: 0.01920277439057827, 9:
                        1.3605794008803018e-12, 10: 1.712225434681465e-23, 11:
                        7.912228231711638e-18, 12: 3.015720722032711e-06, 13:
                        2.558054245857533e-12}}, {'color': {'red': 1.0,
                            'orange': 6.336410618181575e-17, 'blue':
                            2.434076940005525e-09, 'black':
                            7.329084006357789e-10}, 'number': {1:
                                5.376617112927567e-19, 2:
                                2.731435615340415e-14, 3: 0.9999744892120361,
                                4: 4.030155320577933e-09, 5:
                                2.0029796360176988e-05, 6:
                                4.481022642721655e-06, 7:
                                3.3740248982881784e-25, 8:
                                3.135186554814595e-10, 9:
                                9.945608780981274e-07, 10:
                                1.831048401551745e-14, 11:
                                1.0665963474135313e-24, 12:
                                1.1662387346590851e-21, 13:
                                2.6107892715916366e-16}}]))
